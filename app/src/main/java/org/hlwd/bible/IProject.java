
package org.hlwd.bible;

public interface IProject
{
    enum APP_PREF_KEY
    {
        LOG_STATUS,

        INSTALL_STATUS,

        UPDATE_STATUS,

        BIBLE_NAME,

        BIBLE_NAME_DIALOG,

        BOOK_CHAPTER_DIALOG,

        TRAD_BIBLE_NAME,

        BIBLE_ID,

        LAYOUT_DYNAMIC_1,

        LAYOUT_DYNAMIC_2,

        LAYOUT_DYNAMIC_3,

        LAYOUT_DYNAMIC_4,

        THEME_NAME,

        FONT_NAME,

        FONT_SIZE,

        FAV_SYMBOL,

        FAV_ORDER,

        VIEW_POSITION,

        TAB_SELECTED,

        PLAN_ID,

        PLAN_PAGE,

        UI_LAYOUT
    }
}
